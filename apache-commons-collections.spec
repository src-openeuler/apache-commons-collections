%global base_name       collections
%global short_name      commons-%{base_name}

Name:           apache-%{short_name}
Version:        3.2.2
Release:        11
Summary:        A series of new interfaces,implementations and utilities provided for Java Collections
License:        ASL 2.0
URL:            http://commons.apache.org/%{base_name}/
Source0:        http://www.apache.org/dist/commons/%{base_name}/source/%{short_name}-%{version}-src.tar.gz

Patch0001:      0001-Port-to-Java-8.patch

BuildArch:      noarch

BuildRequires:  ant maven-local mvn(org.apache.commons:commons-parent:pom:)
BuildRequires:  mvn(org.apache.maven.plugins:maven-antrun-plugin)

%description
The Java Collections Framework was a major addition in JDK 1.2. It added many powerful 
data structures that accelerate development of most significant Java applications. 
Since that time it has become the recognised standard for collection handling in Java.
Commons-Collections seek to build upon the JDK classes by providing new interfaces, 
implementations and utilities. There are many features, including:
- Bag interface for collections that have a number of copies of each object
- BidiMap interface for maps that can be looked up from value to key as well and key to value
- MapIterator interface to provide simple and quick iteration over maps
- Transforming decorators that alter each object as it is added to the collection
- Composite collections that make multiple collections look like one
- Ordered maps and sets that retain the order elements are added in, including an LRU based map
- Reference map that allows keys and/or values to be garbage collected under close control
- Many comparator implementations
- Many iterator implementations
- Adapter classes from array and enumerations to collections
- Utilities to test or create typical set-theory properties of collections such as union, 
  intersection, and closure

%package devel
Summary:        Libraries and test files for developing applications that use %{name}
Requires:       %{name} = %{version}-%{release}
Provides:       %{name}-testframework = %{version}-%{release} 
Obsoletes:      %{name}-testframework < %{version}-%{release}          

%description devel
Libraries and test files for developing applications that use %{name}.

%package help
Summary:        Help documents for %{name} package
Provides:       %{name}-testframework-javadoc = %{version}-%{release} %{name}-javadoc = %{version}-%{release}
Obsoletes:      %{name}-testframework-javadoc < %{version}-%{release} %{name}-javadoc < %{version}-%{release}

%description help
Help documents for %{name} package.

%prep
%autosetup -n %{short_name}-%{version}-src -p1

find . -name "*.jar" -exec rm -f {} \;
find . -name "*.class" -exec rm -f {} \;

sed -i 's/\r//' LICENSE.txt PROPOSAL.html README.txt NOTICE.txt

%mvn_package :%{short_name}-testframework testframework
%mvn_file ':%{short_name}{,-testframework}' %{name}@1 %{short_name}@1

%build
%mvn_build -- -DskipTests

ant tf.javadoc -Dtf.build.docs=target/site/apidocs/

%mvn_artifact %{short_name}:%{short_name}-testframework:%{version} target/%{short_name}-testframework-%{version}.jar

%install
%mvn_install

%files -f .mfiles
%license LICENSE.txt

%files devel -f .mfiles-testframework

%files help -f .mfiles-javadoc
%doc PROPOSAL.html README.txt NOTICE.txt

%changelog
* Wed Dec 4 2019 wutao <wutao61@huawei.com> - 3.2.2-11
- Package init
